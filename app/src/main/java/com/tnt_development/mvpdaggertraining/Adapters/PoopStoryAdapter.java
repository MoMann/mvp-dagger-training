package com.tnt_development.mvpdaggertraining.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tnt_development.mvpdaggertraining.MVP.Model.Story;
import com.tnt_development.mvpdaggertraining.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by tadejvengust1 on 15. 05. 17.
 */

public class PoopStoryAdapter  extends RecyclerView.Adapter<PoopStoryAdapter.StoryViewHolder>{

    private List<Story> storyList = new ArrayList<>();


    @Inject
    public PoopStoryAdapter() {

    }

    @Override
    public StoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, null);
        StoryViewHolder viewHolder = new StoryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StoryViewHolder holder, int position) {
        Story story = storyList.get(position);

        holder.name.setText(story.getName());
    }

    public void setPoopStories(List<Story> stories)
    {
        this.storyList.addAll(stories);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return storyList.size();
    }

    class StoryViewHolder extends RecyclerView.ViewHolder {

        protected TextView name;

        public StoryViewHolder(View v)
        {
            super(v);
            //TODO: use data biding
            this.name = (TextView) v.findViewById(R.id.tvName);

        }
    }


}
