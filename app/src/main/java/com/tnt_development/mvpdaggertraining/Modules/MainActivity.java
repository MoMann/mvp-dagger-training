package com.tnt_development.mvpdaggertraining.Modules;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.tnt_development.mvpdaggertraining.Adapters.PoopStoryAdapter;
import com.tnt_development.mvpdaggertraining.Application.TestApplication;
import com.tnt_development.mvpdaggertraining.DI.Component.DaggerMainComponent;
import com.tnt_development.mvpdaggertraining.DI.Component.MainComponent;
import com.tnt_development.mvpdaggertraining.DI.Module.MainModule;
import com.tnt_development.mvpdaggertraining.MVP.Model.Story;
import com.tnt_development.mvpdaggertraining.MVP.Presenter.MainPresenter;
import com.tnt_development.mvpdaggertraining.MVP.View.MainView;
import com.tnt_development.mvpdaggertraining.R;
import com.tnt_development.mvpdaggertraining.databinding.ActivityMainBinding;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainView {

    private ActivityMainBinding binding;

    @Inject
    MainPresenter mPresenter;

    @Inject
    PoopStoryAdapter poopStoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainComponent component = DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .testApplicationComponent(TestApplication.get(this).component())
                .build();

        component.injectMainActivity(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setMainActivityHandlers(this);
        initRecyclerView();

        mPresenter.loadStoriesFromApi(0);
    }

    private void initRecyclerView() {
        binding.rvPoopStories.setLayoutManager(new LinearLayoutManager(this));
        binding.rvPoopStories.setAdapter(poopStoryAdapter);

    }


    @Override
    public void loadPoopStories(List<Story> list) {
        poopStoryAdapter.setPoopStories(list);
    }

    @Override
    public void showError(String message) {
        //TODO: popup
    }

    @Override
    public void loadDetailActivity() {
        //TODO: show details and send data using serialization
    }
}
