package com.tnt_development.mvpdaggertraining.MVP.Model;

public class StoryLines {
    private String storyID;
    private String line;
    private String id;

    public String getStoryID() {
        return this.storyID;
    }

    public void setStoryID(String storyID) {
        this.storyID = storyID;
    }

    public String getLine() {
        return this.line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
