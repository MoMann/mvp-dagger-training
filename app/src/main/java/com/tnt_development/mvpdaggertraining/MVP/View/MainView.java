package com.tnt_development.mvpdaggertraining.MVP.View;

import com.tnt_development.mvpdaggertraining.MVP.Model.Story;

import java.util.List;

/**
 * Created by tadejvengust1 on 15. 05. 17.
 */

public interface MainView {

    void loadPoopStories(List<Story> story);

    void showError(String message);

    void loadDetailActivity();
}
