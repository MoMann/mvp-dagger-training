package com.tnt_development.mvpdaggertraining.MVP.Model;

public class Story {
    private String storyTypeID;
    private String name;
    private String active;
    private String id;
    private String seen;
    private com.tnt_development.mvpdaggertraining.MVP.Model.StoryLines[] StoryLines;

    public String getStoryTypeID() {
        return this.storyTypeID;
    }

    public void setStoryTypeID(String storyTypeID) {
        this.storyTypeID = storyTypeID;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return this.active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSeen() {
        return this.seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public com.tnt_development.mvpdaggertraining.MVP.Model.StoryLines[] getStoryLines() {
        return this.StoryLines;
    }

    public void setStoryLines(com.tnt_development.mvpdaggertraining.MVP.Model.StoryLines[] StoryLines) {
        this.StoryLines = StoryLines;
    }
}
