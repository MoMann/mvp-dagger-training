package com.tnt_development.mvpdaggertraining.MVP.Presenter;

import com.tnt_development.mvpdaggertraining.MVP.Model.Story;
import com.tnt_development.mvpdaggertraining.MVP.View.MainView;
import com.tnt_development.mvpdaggertraining.Service.PoopStoryApi;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tadejvengust1 on 15. 05. 17.
 */

public class MainPresenter {


    PoopStoryApi poopStoryApi;

    MainView view;

    @Inject
    public MainPresenter(PoopStoryApi poopStoryApi, MainView view) {
        this.poopStoryApi = poopStoryApi;
        this.view = view;
    }

    public void loadStoriesFromApi(int id)
    {
        //use rxJava
        Call<List<Story>> call = poopStoryApi.getStories(id);
        call.enqueue(new Callback<List<Story>>() {
            @Override
            public void onResponse(Call<List<Story>> call, Response<List<Story>> response) {
                view.loadPoopStories(response.body());
            }

            @Override
            public void onFailure(Call<List<Story>> call, Throwable t) {
                view.showError(t.getMessage());
            }
        });
    }

}
