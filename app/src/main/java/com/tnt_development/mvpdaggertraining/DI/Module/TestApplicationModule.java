package com.tnt_development.mvpdaggertraining.DI.Module;

import com.tnt_development.mvpdaggertraining.Application.Static;
import com.tnt_development.mvpdaggertraining.DI.Scope.ApplicationScope;
import com.tnt_development.mvpdaggertraining.Service.PoopStoryApi;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tadejvengust1 on 14. 05. 17.
 */

@Module
public class TestApplicationModule {

    @Provides
    @ApplicationScope
    GsonConverterFactory converterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @ApplicationScope
    OkHttpClient okHttpClient(){
        return new OkHttpClient.Builder().build();
    }

    @Provides
    @ApplicationScope
    public Retrofit retrofit(OkHttpClient okHttpClient, GsonConverterFactory converterFactory){
        return new Retrofit.Builder()
                 .baseUrl(Static.baseUrl)
                .addConverterFactory(converterFactory)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @ApplicationScope
    public PoopStoryApi poopStoryApi(Retrofit retrofit) {
        return retrofit.create(PoopStoryApi.class);
    }
}
