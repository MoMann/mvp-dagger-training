package com.tnt_development.mvpdaggertraining.DI.Module;

import com.tnt_development.mvpdaggertraining.Adapters.PoopStoryAdapter;
import com.tnt_development.mvpdaggertraining.DI.Scope.ActivityScope;
import com.tnt_development.mvpdaggertraining.MVP.Presenter.MainPresenter;
import com.tnt_development.mvpdaggertraining.MVP.View.MainView;
import com.tnt_development.mvpdaggertraining.Service.PoopStoryApi;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tadejvengust1 on 15. 05. 17.
 */

@ActivityScope
@Module
public class MainModule {

    MainView mainView;

    public MainModule(MainView mainView) {
        this.mainView = mainView;
    }

//    @Provides
//    @ActivityScope
//    PoopStoryAdapter poopStoryAdapter(){
//        return new PoopStoryAdapter();
//    }


    //TODO: check if there is a better way
    @Provides
    @ActivityScope
    MainView mainView(){
        return mainView;
    }


}
