package com.tnt_development.mvpdaggertraining.DI.Component;

import com.tnt_development.mvpdaggertraining.DI.Module.TestApplicationModule;
import com.tnt_development.mvpdaggertraining.DI.Scope.ApplicationScope;
import com.tnt_development.mvpdaggertraining.MVP.Presenter.MainPresenter;
import com.tnt_development.mvpdaggertraining.Modules.MainActivity;
import com.tnt_development.mvpdaggertraining.Service.PoopStoryApi;

import dagger.Component;

/**
 * Created by tadejvengust1 on 14. 05. 17.
 */
@ApplicationScope
@Component(modules = TestApplicationModule.class)
public interface TestApplicationComponent {


    PoopStoryApi getPoopStoryApi();

}
