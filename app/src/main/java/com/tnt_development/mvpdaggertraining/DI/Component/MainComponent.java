package com.tnt_development.mvpdaggertraining.DI.Component;

import com.tnt_development.mvpdaggertraining.DI.Module.MainModule;
import com.tnt_development.mvpdaggertraining.DI.Module.TestApplicationModule;
import com.tnt_development.mvpdaggertraining.DI.Scope.ActivityScope;
import com.tnt_development.mvpdaggertraining.MVP.Presenter.MainPresenter;
import com.tnt_development.mvpdaggertraining.MVP.View.MainView;
import com.tnt_development.mvpdaggertraining.Modules.MainActivity;
import com.tnt_development.mvpdaggertraining.Service.PoopStoryApi;

import dagger.Component;
import dagger.Module;

/**
 * Created by tadejvengust1 on 15. 05. 17.
 */

@ActivityScope
@Component (modules = MainModule.class, dependencies = TestApplicationComponent.class)
public interface MainComponent {

    void injectMainActivity(MainActivity activity);

    MainView getMainView();

}
