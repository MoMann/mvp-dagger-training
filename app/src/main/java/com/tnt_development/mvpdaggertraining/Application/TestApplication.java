package com.tnt_development.mvpdaggertraining.Application;

import android.app.Activity;
import android.app.Application;

import com.tnt_development.mvpdaggertraining.DI.Component.DaggerTestApplicationComponent;
import com.tnt_development.mvpdaggertraining.DI.Component.TestApplicationComponent;


/**
 * Created by tadejvengust1 on 14. 05. 17.
 */

public class TestApplication extends Application {

    private TestApplicationComponent component;

    @Override
    public void onCreate() {

        super.onCreate();

        component = DaggerTestApplicationComponent.builder().build();

    }

    public static TestApplication get (Activity activity)
    {
        return (TestApplication)activity.getApplication();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public TestApplicationComponent component(){
        return component;
    }
}
