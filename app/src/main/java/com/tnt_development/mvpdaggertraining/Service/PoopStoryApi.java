package com.tnt_development.mvpdaggertraining.Service;

import com.tnt_development.mvpdaggertraining.MVP.Model.Story;

import java.util.List;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by tadejvengust1 on 14. 05. 17.
 */

public interface PoopStoryApi {

    @GET("/poop-money/padmin/getStoryLines.php")
    Call<List<Story>> getStories(@Query("id") int id);
}
